import unittest
from pynkins import one

class TestMain(unittest.TestCase):
    def testOne_success(self):
        res = one()
        self.assertEqual(res, 1)

    def testOne_failure(self):
        res = one()
        self.assertNotEqual(res, 2)
